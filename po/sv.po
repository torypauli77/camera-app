# Swedish translation for camera-app
# Copyright © 2013-2014 Rosetta Contributors and Canonical Ltd 2013-2014
# This file is distributed under the same license as the camera-app package.
# Josef Andersson <josef.andersson@fripost.org>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-23 17:57+0000\n"
"PO-Revision-Date: 2021-06-04 23:45+0000\n"
"Last-Translator: Marcus Fredlund <marcus.karlson@gmail.com>\n"
"Language-Team: Swedish <https://translate.ubports.com/projects/ubports/"
"camera-app/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: ../AdvancedOptions.qml:16
msgid "Settings"
msgstr "Inställningar"

#: ../AdvancedOptions.qml:48
msgid "Add date stamp on captured images"
msgstr "Lägg till datum på tagna bilder"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:70
msgid "Format"
msgstr "Format"

#: ../AdvancedOptions.qml:122
msgid "Date formatting keywords"
msgstr "Nyckelord för datum format"

#: ../AdvancedOptions.qml:127
msgid "the day as number without a leading zero (1 to 31)"
msgstr "Ange dagen som ett nummer utan en nolla i början (1 till 31)"

#: ../AdvancedOptions.qml:128
msgid "the day as number with a leading zero (01 to 31)"
msgstr "ange dagen som ett nummer med en nolla i början (01 till 31)"

#: ../AdvancedOptions.qml:129
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "förkortningen för dagens namn (\"mån\" till \"sön\")."

#: ../AdvancedOptions.qml:130
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr "långt lokaliserat namn på dagens namn (t. ex. 'Måndag' till 'Söndag')."

#: ../AdvancedOptions.qml:131
msgid "the month as number without a leading zero (1 to 12)"
msgstr "månaden som nummer utan en nolla i början (1 till 12)"

#: ../AdvancedOptions.qml:132
msgid "the month as number with a leading zero (01 to 12)"
msgstr "månaden som ett nummer med en nolla i början (01 till 12)"

#: ../AdvancedOptions.qml:133
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "begränsat lokaliserat namn på månadens namn (t. ex. 'Jan' till 'Dec')."

#: ../AdvancedOptions.qml:134
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr ""
"långt lokaliserat namn på månadens namn(t. ex. 'Januari' till 'December')."

#: ../AdvancedOptions.qml:135
msgid "the year as two digit number (00 to 99)"
msgstr "året som ett tvåsiffrigt nummer (00 till 99)"

#: ../AdvancedOptions.qml:136
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr ""
"året som ett fyrsiffrigt nummer. Om året är negativt, så ska det föregås med "
"ett minus tecken."

#: ../AdvancedOptions.qml:137
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr ""
"timmarna utan en nolla i början (0 till 23 eller 1 till 12 om AM/PM visas)"

#: ../AdvancedOptions.qml:138
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr ""
"timmarna med en nolla i början (00 till 23 eller 01 till 12 om AM/PM visas)"

#: ../AdvancedOptions.qml:139
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr "timmarna utan en nolla i början (0 till 23, även om AM/PM visas)"

#: ../AdvancedOptions.qml:140
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr "timmarna med en nolla i början (00 till 23, även om AM/PM visas)"

#: ../AdvancedOptions.qml:141
msgid "the minute without a leading zero (0 to 59)"
msgstr "minuterna utan en nolla i början (0 till 59)"

#: ../AdvancedOptions.qml:142
msgid "the minute with a leading zero (00 to 59)"
msgstr "minuterna med en nolla i början (00 till 59)"

#: ../AdvancedOptions.qml:143
msgid "the second without a leading zero (0 to 59)"
msgstr "sekunderna utan en nolla i början (0 till 59)"

#: ../AdvancedOptions.qml:144
msgid "the second with a leading zero (00 to 59)"
msgstr "sekunderna med en en nolla i början (0 till 59)"

#: ../AdvancedOptions.qml:145
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "millisekunderna utan nollor i början (0 till 999)"

#: ../AdvancedOptions.qml:146
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "millisekunderna med nollor i början (000 till 999)"

#: ../AdvancedOptions.qml:147
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "visa AM/PM. AP ersätts med antingen 'AM' eller 'PM'."

#: ../AdvancedOptions.qml:148
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr "visa am/pm. ap ersätts med antingen 'am' eller 'pm'."

#: ../AdvancedOptions.qml:149
msgid "the timezone (for example 'CEST')"
msgstr "tidszonen (till exempel 'CEST')"

#: ../AdvancedOptions.qml:158
msgid "Add to Format"
msgstr "Lägg till i Format"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: ../AdvancedOptions.qml:189
msgid "Color"
msgstr "Färg"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: ../AdvancedOptions.qml:257
msgid "Alignment"
msgstr "Placering"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:303
msgid "Opacity"
msgstr "Opacitet"

#: ../DeleteDialog.qml:24
msgid "Delete media?"
msgstr "Ta bort media?"

#: ../DeleteDialog.qml:34 ../PhotogridView.qml:56 ../SlideshowView.qml:75
msgid "Delete"
msgstr "Radera"

#: ../DeleteDialog.qml:40 ../ViewFinderOverlay.qml:1122
#: ../ViewFinderOverlay.qml:1136 ../ViewFinderOverlay.qml:1175
#: ../ViewFinderView.qml:494
msgid "Cancel"
msgstr "Avbryt"

#: ../GalleryView.qml:200
msgid "No media available."
msgstr "Inget media tillgängligt."

#: ../GalleryView.qml:235
msgid "Scanning for content..."
msgstr "Söker efter innehåll..."

#: ../GalleryViewHeader.qml:82 ../SlideshowView.qml:45
msgid "Select"
msgstr "Välj"

#: ../GalleryViewHeader.qml:83
msgid "Edit Photo"
msgstr "Redigera foto"

#: ../GalleryViewHeader.qml:83
msgid "Photo Roll"
msgstr "Fotovisare"

#: ../Information.qml:20
msgid "About"
msgstr "Om"

#: ../Information.qml:82
msgid "Get the source"
msgstr "Hämta källan"

#: ../Information.qml:83
msgid "Report issues"
msgstr "Rapportera problem"

#: ../Information.qml:84
msgid "Help translate"
msgstr "Hjälp till med översättning"

#: ../MediaInfoPopover.qml:25
msgid "Media Information"
msgstr "Media Information"

#: ../MediaInfoPopover.qml:30
#, qt-format
msgid "Name : %1"
msgstr "Namn: %1"

#: ../MediaInfoPopover.qml:33
#, qt-format
msgid "Type : %1"
msgstr "Typ: %1"

#: ../MediaInfoPopover.qml:37
#, qt-format
msgid "Width : %1"
msgstr "Bredd: %1"

#: ../MediaInfoPopover.qml:41
#, qt-format
msgid "Height : %1"
msgstr "Höjd: %1"

#: ../NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "Enheten full, frigör utrymme för att fortsätta."

#: ../PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "Svep vänster för bildrullning"

#: ../PhotogridView.qml:42 ../SlideshowView.qml:60
msgid "Share"
msgstr "Dela"

#: ../PhotogridView.qml:69 ../SlideshowView.qml:53
msgid "Gallery"
msgstr "Galleri"

#: ../SlideshowView.qml:68
msgid "Image Info"
msgstr "Bildinfo"

#: ../SlideshowView.qml:86
msgid "Edit"
msgstr "Redigera"

#: ../SlideshowView.qml:441
msgid "Back to Photo roll"
msgstr "Återgå till bildspel"

#: ../UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Kunde inte dela"

#: ../UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Kunde inte dela foton och videor samtidigt"

#: ../UnableShareDialog.qml:30
msgid "Ok"
msgstr "Ok"

#: ../ViewFinderOverlay.qml:367 ../ViewFinderOverlay.qml:390
#: ../ViewFinderOverlay.qml:418 ../ViewFinderOverlay.qml:441
#: ../ViewFinderOverlay.qml:526 ../ViewFinderOverlay.qml:589
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:42
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:65
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:86
msgid "On"
msgstr "På"

#: ../ViewFinderOverlay.qml:372 ../ViewFinderOverlay.qml:400
#: ../ViewFinderOverlay.qml:423 ../ViewFinderOverlay.qml:446
#: ../ViewFinderOverlay.qml:465 ../ViewFinderOverlay.qml:531
#: ../ViewFinderOverlay.qml:599
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:47
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:70
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:91
msgid "Off"
msgstr "Av"

#: ../ViewFinderOverlay.qml:395
msgid "Auto"
msgstr "Auto"

#: ../ViewFinderOverlay.qml:432
msgid "HDR"
msgstr "HDR"

#: ../ViewFinderOverlay.qml:470
msgid "5 seconds"
msgstr "5 sekunder"

#: ../ViewFinderOverlay.qml:475
msgid "15 seconds"
msgstr "15 sekunder"

#: ../ViewFinderOverlay.qml:493
msgid "Fine Quality"
msgstr "Bra kvalitet"

#: ../ViewFinderOverlay.qml:498
msgid "High Quality"
msgstr "Hög kvalitet"

#: ../ViewFinderOverlay.qml:503
msgid "Normal Quality"
msgstr "Normalkvalitet"

#: ../ViewFinderOverlay.qml:508
msgid "Basic Quality"
msgstr "Grundkvalitet"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ../ViewFinderOverlay.qml:541
msgid "SD"
msgstr "SD"

#: ../ViewFinderOverlay.qml:549
msgid "Save to SD Card"
msgstr "Spara till SD-kort"

#: ../ViewFinderOverlay.qml:554
msgid "Save internally"
msgstr "Spara internt"

#: ../ViewFinderOverlay.qml:594
msgid "Vibrate"
msgstr "Vibrera"

#: ../ViewFinderOverlay.qml:1119
msgid "Low storage space"
msgstr "Lågt lagringsutrymme"

#: ../ViewFinderOverlay.qml:1120
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr ""
"Ditt lagringsutrymme håller på att ta slut. För att fortsätta utan avbrott, "
"frigör utrymme nu."

#: ../ViewFinderOverlay.qml:1133
msgid "External storage not writeable"
msgstr "Externt lagringsmedia ej skrivbart"

#: ../ViewFinderOverlay.qml:1134
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"Det verkar inte gå att skriva till ditt externa lagringsmedia. Att ta ut och "
"sätta in det igen kan kanske lösa problemet, eller så kan det behöva "
"formateras om."

#: ../ViewFinderOverlay.qml:1172
msgid "Cannot access camera"
msgstr "Kan inte komma åt kamera"

#: ../ViewFinderOverlay.qml:1173
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"Kameraprogrammet har inte behörighet att komma åt kamerahårdvaran, eller ett "
"annat fel uppstod.\n"
"\n"
"Om det inte löser problemet att ge behörighet, starta om din telefon."

#: ../ViewFinderOverlay.qml:1182
msgid "Edit Permissions"
msgstr "Redigera behörigheter"

#: ../ViewFinderView.qml:485
msgid "Capture failed"
msgstr "Det gick inte att ta bilden"

#: ../ViewFinderView.qml:490
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"Att byta ut ditt externa media, formatera det eller starta om enheten kanske "
"löser problemet."

#: ../ViewFinderView.qml:491
msgid "Restarting your device might fix the problem."
msgstr "Försök starta om din enhet för att lösa problemet."

#: ../camera-app.qml:57
msgid "Flash"
msgstr "Blixt"

#: ../camera-app.qml:58
msgid "Light;Dark"
msgstr "Ljust;Mörkt"

#: ../camera-app.qml:61
msgid "Flip Camera"
msgstr "Vänd kameran"

#: ../camera-app.qml:62
msgid "Front Facing;Back Facing"
msgstr "Framåtvänd;Bakåtvänd"

#: ../camera-app.qml:65
msgid "Shutter"
msgstr "Slutare"

#: ../camera-app.qml:66
msgid "Take a Photo;Snap;Record"
msgstr "Ta en bild;Knäpp;Spela in"

#: ../camera-app.qml:69
msgid "Mode"
msgstr "Läge"

#: ../camera-app.qml:70
msgid "Stills;Video"
msgstr "Stillbild;Film"

#: ../camera-app.qml:74
msgid "White Balance"
msgstr "Vitbalans"

#: ../camera-app.qml:75
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "Ljusförhållande;Dagsljus;Molnigt;Inomhus"

#: ../camera-app.qml:380
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "<b>%1</b> foton tagna idag"

#: ../camera-app.qml:381
msgid "No photos taken today"
msgstr "Inga foton tagna idag"

#: ../camera-app.qml:391
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "<b>%1</b> filmklipp tagna idag"

#: ../camera-app.qml:392
msgid "No videos recorded today"
msgstr "Inga filmklipp tagna idag"

#: camera-app.desktop.in.in.h:1
msgid "Camera"
msgstr "Kamera"

#: camera-app.desktop.in.in.h:2
msgid "Camera application"
msgstr "Kameraprogram"

#: camera-app.desktop.in.in.h:3
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "Foton;Videor;Spela in;Ögonblicksbild"
